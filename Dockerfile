FROM python:3.12

WORKDIR /app

COPY requirements.txt /app/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

COPY main.py .
COPY db.py .
COPY cz_routes.py .
COPY src src
COPY static static
COPY templates templates

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]