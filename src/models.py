from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.ext.asyncio import AsyncAttrs
from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy import String, Column, Text, Boolean
from sqlalchemy.dialects.postgresql import JSONB

from enum import Enum

from sqlalchemy.dialects.postgresql import ENUM as PgEnum

class Base(DeclarativeBase, AsyncAttrs):
    pass

class FlightStatusEnum(Enum):
    completed = 'completed'
    analysis = 'analysis'
    analysing = 'analysing'
    planning = 'planning'

class Flight(Base):
    __tablename__ = 'flight'
    __table_args__ = {"schema": "planning"}
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(String(255))
    flight_status = Column(PgEnum(FlightStatusEnum, name='enum_flight_status', create_type=False, schema='planning'), nullable=False)

    def __repr__(self) -> str:
        return f"Flight(id={self.id!r}, name={self.name!r}, flight_status={self.flight_status!r})"

class Trace(Base):
    __tablename__ = 'trace'
    id: Mapped[int] = mapped_column(primary_key=True)
    trace: Mapped[str] = mapped_column(String(255))
    message: Mapped[str] = mapped_column(String(255))
    payload: Mapped[str] = mapped_column(JSONB())
    error: Mapped[bool] = mapped_column(Boolean())
