import os

DB_HOST = os.getenv('DB_HOST', 'localhost')
DB_PORT = os.getenv('DB_PORT', 5432)
DB_USER = os.getenv('DB_USER', 'postgres')
DB_PASSWORD = os.getenv('DB_PASSWORD')
DB_NAME = os.getenv('DB_NAME', 'cz')

# Default - results is subdirectory of images
IMAGES_BASE_DIR = os.getenv('IMAGES_BASE_DIR', '/images')
RESULTS_BASE_DIR = os.getenv('RESULTS_BASE_DIR', '/images/results')
