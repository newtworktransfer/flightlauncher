import json

import psycopg2
import sys
import os

import argparse

from datetime import datetime, timezone

################################################################################################################
# SELECTING
################################################################################################################

# def select_flight_object(conn, flight_id):
#     with conn.cursor() as cursor:
#         cursor.execute('SELECT * FROM planning.flight_object WHERE flight_id = %s AND support_id IS NOT NULL', (flight_id,))
#         return cursor.fetchall()

DB_HOST = '10.24.65.227'
# DB_HOST = 'localhost'
DB_USER = 'postgres'
DB_PASSWORD = 'postgres'
DB_NAME = 'cz'

def select_all_flights(conn):
    with conn.cursor() as cursor:
        cursor.execute('SELECT id, name, flight_status FROM planning.flight ORDER BY id')
        return cursor.fetchall(), ['id', 'name', 'flight_status']

def select_image_files(conn, flight_id):
    sql = """
            SELECT
                image_file.id, path, flight_object.flight_id
            FROM
                inspection.image_file AS image_file
                LEFT JOIN planning.flight_object AS flight_object ON image_file.flight_object_id = flight_object.id
            WHERE
                flight_object.flight_id = %s
        """
    with conn.cursor() as cursor:
        cursor.execute(sql, (flight_id,))
        return cursor.fetchall(), ['id', 'path', 'flight_id']

def select_all_tasks(conn):
    with conn.cursor() as cursor:
        cursor.execute('SELECT id, status FROM analysis.task')
        return cursor.fetchall(), ['id', 'status']

def select_supports(conn, line_id):
    sql_string = """
        SELECT
            id, line_id, name, number
        FROM
            reference.support
        WHERE
            line_id = %s
    """
    with conn.cursor() as cursor:
        cursor.execute(sql_string, (line_id,))
        return cursor.fetchall(), ['id', 'line_id', 'name', 'number']

################################################################################################################
# INSERTING
################################################################################################################

LINE_ID = 1
UAV_ID = 1
JOB_ID = 1

def insert_flight(conn, flight_name, job_id, shooting_type_id=None):
    sql_string = 'INSERT INTO planning.flight (name, planned_flight_date, flight_status, job_id, uav_id, shooting_type_id) VALUES (%s, %s, %s, %s, %s, %s) RETURNING id;'
    # dt = datetime.now(timezone.utc)
    dt = datetime.now()
    with conn.cursor() as cursor:
        cursor.execute(sql_string, (flight_name, dt, 'planning', job_id, UAV_ID, shooting_type_id))
        flight_id = cursor.fetchone()[0]
        return flight_id

def insert_flight_object(conn, flight_id, support_id=None, span_id=None):
    sql_string = 'INSERT INTO planning.flight_object (flight_id, support_id, span_id) VALUES (%s, %s, %s) RETURNING id;'
    with conn.cursor() as cursor:
        cursor.execute(sql_string, (flight_id, support_id, span_id))
        flight_object_id = cursor.fetchone()[0]
        return flight_object_id

def insert_image_files(conn, flight_object_id, images):
    if len(images) <= 0:
        raise Exception('Drectory have not got any files')
    sql_string = 'INSERT INTO inspection.image_file (path, flight_object_id) VALUES %s RETURNING id;'
    with conn.cursor() as cursor:
        args = ','.join(cursor.mogrify("(%s, %s)", (path, flight_object_id)).decode('utf-8') for idx, path in enumerate(images))
        cursor.execute(sql_string % args)
        return cursor.fetchall()

def insert_job(conn):
    sql_string = 'INSERT INTO planning.job (name, planned_start_date, planned_end_date, job_status, line_id, update_date) VALUES (%s, %s, %s, %s, %s, %s) RETURNING id;'
    with conn.cursor() as cursor:
        dt = datetime.now(timezone.utc)
        cursor.execute(sql_string, ('job-for-testing', dt, dt, 'planning', LINE_ID, dt))
        return cursor.fetchone()[0]

def get_images_by_flight(conn, flight_id, offset = 0, limit = 32767):
    with conn.cursor() as cursor:
        cursor.execute('''
            SELECT
                img.id, path,
                focal_length, exposure,
                altitude, latitude, longitude,
                flight_yaw, flight_pitch, flight_roll,
                camera_yaw, camera_pitch, camera_roll,
                shooting_angle_code, flight_object_id, error
            FROM
                inspection.image_file as img LEFT JOIN planning.flight_object as flgobj ON img.flight_object_id = flgobj.id
            WHERE
                flgobj.flight_id = %s
            ORDER BY img.id
            OFFSET %s LIMIT %s
        ''', (flight_id, offset, limit))
        flights = cursor.fetchall()

    return [{
        'id': f[0],
        'path': f[1],
        'focal_length': f[2],
        'exposure': f[3],
        'altitude': f[4],
        'latitude': f[5],
        'longitude': f[6],
        'flight_yaw': f[7],
        'flight_pitch': f[8],
        'flight_roll': f[9],
        'camera_yaw': f[10],
        'camera_pitch': f[11],
        'camera_roll': f[12],
        'shooting_angle_code': f[13],
        'flight_object_id': f[14],
        'error': f[15],
    }
    for f in flights]

def get_images_count_by_flight(conn, flight_id):
    with conn.cursor() as cursor:
        cursor.execute('''
            SELECT COUNT(1)
            FROM
                inspection.image_file as img
                    LEFT JOIN planning.flight_object as flgobj ON img.flight_object_id = flgobj.id
            WHERE
                flgobj.flight_id = %s;
        ''', (flight_id,))
        return cursor.fetchone()[0]

def truncate_tasks(conn):
    with conn.cursor() as cursor:
        cursor.execute('TRUNCATE analysis.task CASCADE')
        cursor.execute('UPDATE inspection.image_file SET error = NULL, shooting_angle_code = NULL')

def get_flight_info(conn, flight_id):
    with conn.cursor() as cursor:
        cursor.execute('''
            SELECT
                st.name, st.description
            FROM
                planning.flight as f
                    LEFT JOIN reference.shooting_type as st ON f.shooting_type_id = st.id
            WHERE
                f.id = %s;
        ''', (flight_id,))
        shooting_name, shooting_description = cursor.fetchone()

        cursor.execute('''
            SELECT
                COUNT(1)
            FROM
                planning.flight_object as f
            WHERE
                f.flight_id = %s;
        ''', (flight_id,))
        flight_objects_count = cursor.fetchone()[0]

    return {
        'shooting_type': f'{shooting_name} ({shooting_description})' if shooting_name else 'Не указан',
        'flight_objects_count': flight_objects_count,
    }

def get_image_defects(conn, flight_id, image_file_id):
    with conn.cursor() as cursor:
        cursor.execute('''
            SELECT DISTINCT
                dt.code AS defect_code, defect.id AS defect_id, dt.name
            FROM
                analysis.defect_image_processing_result AS dipr
                LEFT JOIN analysis.defect AS defect ON dipr.defect_id = defect.id
                LEFT JOIN reference.defect_type AS dt ON defect.defect_type_id = dt.id
                LEFT JOIN analysis.image_processing_result AS ipr ON dipr.image_processing_result_id = ipr.id
            WHERE
                ipr.image_file_id = %s
                AND defect.task_id = (SELECT max(id) FROM analysis.task WHERE flight_id = %s)
        ''', (image_file_id, flight_id))
        return [{
            'code': code,
            'defect_id': defect_id,
            'name': name,
        }
        for code, defect_id, name in cursor.fetchall()]

def get_images_required_checks(conn, flight_id):
    with conn.cursor() as cursor:
        cursor.execute('''
        select
            image_file_id, code, name
        from analysis.image_analysed_defect_type as idt
        left join reference.defect_type as dt on idt.defect_code = dt.code
        where task_id = (SELECT max(id) FROM analysis.task WHERE flight_id = %s);
        ''', (flight_id,))

        images = {}
        for image_id, code, name in cursor.fetchall():
            if image_id not in images:
                images[image_id] = []
            images[image_id].append({ 'code': code, 'name': name })
        return images

def dictize(values: tuple, columns: list[str]):
    assert len(values) == 0 or len(values[0]) == len(columns)
    return [
        { key: value[i] for i, key in enumerate(columns) }
        for value in values
    ]

def get_shooting_types(conn):
    with conn.cursor() as cursor:
        cursor.execute('''
            SELECT id, name, description FROM reference.shooting_type;
        ''')
        return dictize(cursor.fetchall(), ('id', 'name', 'description'))

def get_lines(conn):
    with conn.cursor() as cursor:
        cursor.execute('''
            SELECT id, line_id, name, latitude, longitude  FROM reference.support;
        ''')

        lines = {}
        for id, line_id, name, latitude, longitude in cursor.fetchall():
            if line_id not in lines:
                lines[line_id] = []
            lines[line_id].append({
                'id': id,
                'name': name,
                'latitude': latitude,
                'longitude': longitude
            })
        return lines

def append_supports(conn, flight_id, line_id):
    with conn.cursor() as cursor:
        cursor.execute('SELECT id FROM reference.support WHERE line_id = %s', (line_id,))
        supports = cursor.fetchall()
        for support_id in supports:
            sql_string = 'INSERT INTO planning.flight_object (flight_id, support_id) VALUES (%s, %s)'
            cursor.execute(sql_string, (flight_id, support_id))

        cursor.execute('SELECT id FROM reference.span WHERE line_id = %s', (line_id,))
        spans = cursor.fetchall()
        for span_id in spans:
            sql_string = 'INSERT INTO planning.flight_object (flight_id, span_id) VALUES (%s, %s)'
            cursor.execute(sql_string, (flight_id, span_id))


def select_image_file_path(conn, image_file_id):
    sql = """
            SELECT path FROM inspection.image_file
            WHERE id = %s
        """
    with conn.cursor() as cursor:
        cursor.execute(sql, (image_file_id,))
        return cursor.fetchone()[0]

def get_image_paths(base_dir: str, rel_dir=None):
    """
    Return list of image paths.

    base_dir: directory with images (and subdirectories with images)
    rel_dir: name of directory for splitting path

    Example:
    /wsl/images/2.4/flight/image1.jpg with rel_dir='images' will be transformed to 2.4/flight/image1.jpg
    """
    result_filenames = []
    for path, _, filenames in os.walk(base_dir):
        if not filenames:
            continue

        if rel_dir is not None:
            dirs = path.split(os.sep)
            path = os.path.join(*dirs[dirs.index(rel_dir) + 1:])

        filenames = [
            *map(lambda filename: os.path.join(path, filename),
                filter(lambda filename: filename.lower().endswith(('.png', '.jpg', '.jpeg')), filenames))
        ]

        result_filenames += filenames

    return result_filenames

def insert_pipeline(conn, image_dir, flight_name, job_id, rel_dir=None, thermal=False, shooting_type_id=None):
    images = get_image_paths(image_dir, rel_dir=rel_dir)

    print(*images, sep='\n')

    flight_id = insert_flight(conn, flight_name, job_id, shooting_type_id=shooting_type_id)
    print('INSERTED FLIGHT ID: {} NAME: {}'.format(flight_id, flight_name))
    flight_object_id = insert_flight_object(conn, flight_id)
    print('INSERTED FLIGHT OBJECT ID:', flight_object_id)
    image_file_ids = insert_image_files(conn, flight_object_id, images)
    print('INSERTED IMAGE FILE IDS:', image_file_ids)
    print(f'INSERTED {len(images)} IMAGES')
    return flight_id, images

def create_command(conn, args):
    if args.target == 'job':
        job_id = insert_job(conn)
        print('INSERTED JOB ID:', job_id)

    elif args.target == 'flight':
        if not args.flight_name: return print('Specify --flight-name')
        job_id = args.job_id if args.job_id is not None else JOB_ID

        flight_id = insert_flight(conn, args.flight_name, job_id)
        print('INSERTED FLIGHT ID: ', flight_id)


    elif args.target == 'flight_object':
        if not args.flight_id: return print('Specify --flight-id')

        flight_object_id = insert_flight_object(conn, args.flight_id)
        print('INSERTED FLIGHT OBJECT ID', flight_object_id)


    # elif args.target == 'image_file':
    #     if not args.flight_object_id: return print('Specify --flight-object-id')
    #     if not args.image_dir: return print('Specify --image-dir')

    #     images = get_recurrent_filenames(args.image_dir)
    #     insert_image_files(conn, args.flight_object_id, images)


    elif args.target == 'pipeline':
        if not args.image_dir: return print('Specify --image-dir')

        image_dir = args.image_dir
        flight_name = args.flight_name if args.flight_name is not None else image_dir
        job_id = args.job_id if args.job_id is not None else JOB_ID

        insert_pipeline(conn, image_dir, flight_name, job_id, rel_dir=args.rel_dir, thermal=args.thermal)

    elif args.target == 'pipeline_recurrent':
        if not args.image_dir: return print('Specify --image-dir')

        image_dir = args.image_dir
        job_id = args.job_id if args.job_id is not None else JOB_ID

        for dir in os.scandir(image_dir):
            if dir.is_dir():
                insert_pipeline(conn, dir.path, '/'.join(dir.path.split('/')[-2:]), job_id, rel_dir=args.rel_dir, thermal=args.thermal)

    else:
        return print('Specify target (job, flight, image_file, full_image_pipeline)')

def ls_command(conn, args):
    if args.target == 'flight':
        items, cols = select_all_flights(conn)
        print('{:4} | {:120} | {}'.format(*cols))
        for item in items:
            print('{:4} | {:120} | {}'.format(*item))
    
    elif args.target == 'task':
        items, cols = select_all_tasks(conn)
        print('{} | {}'.format(*cols))
        for item in items:
            print('{} | {}'.format(*item))

    else:
        return print('Specify --target (flight, image_file, task)')

def update_command(conn, args):
    if args.target != 'flights':
        return print('Implemeted only all flights updating')
    
    sql_string = """
        UPDATE planning.flight SET flight_status = 'analysis'
    """
    with conn.cursor() as cursor:
        cursor.execute(sql_string)

def append_command(conn, args):
    if not args.flight_id: return print('Specify --flight-id')

    flight_id = args.flight_id
    line_id = args.line_id if args.line_id is not None else LINE_ID

    supports, _ = select_supports(conn, line_id)

    print(*supports, sep='\n')
    yes = input(f'Are you sure to add next supports to flight {flight_id}? (y/n): ')
    if (yes != 'y'): return print('Operation canceled')

    for support_id, _, _, _ in supports:
        flight_object_id = insert_flight_object(conn, flight_id, support_id=support_id)
        print('INSERTED FLIGHT OBJECT ID:', flight_object_id)

def launch_command(conn, args):
    if not args.flight_id: return print('Specify --flight-id')

    flight_id = args.flight_id
    sql_string = "UPDATE planning.flight SET flight_status = 'analysis' WHERE id = %s"

    with conn.cursor() as cursor:
        cursor.execute(sql_string, (flight_id,))

def main():
    parser = argparse.ArgumentParser(description='Utility')
    parser.add_argument('command', type=str)
    parser.add_argument('target', type=str)

    parser.add_argument('-f', '--flight-id', type=int)
    parser.add_argument('-j', '--job-id', type=int)
    parser.add_argument('-l', '--line-id', type=int)
    parser.add_argument('--thermal', default=False, action='store_true')

    parser.add_argument('--flight-object-id', type=int)
    parser.add_argument('-i', '--image-dir', type=str)
    parser.add_argument('-n', '--flight-name', type=str)
    parser.add_argument('-r', '--rel-dir', type=str)


    args = parser.parse_args()
    with psycopg2.connect(dbname=DB_NAME, user=DB_USER, password=DB_PASSWORD, host=DB_HOST, port=5432) as conn:
        conn.set_client_encoding('UTF8')
        if args.command == 'ls':
            ls_command(conn, args)
        elif args.command == 'insert':
            create_command(conn, args)
        elif args.command == 'up':
            update_command(conn, args)
        elif args.command in ('append', 'a'):
            append_command(conn, args)
        elif args.command == 'launch':
            launch_command(conn, args)
        else:
            print('unknown command')

if __name__ == '__main__':
    main()
