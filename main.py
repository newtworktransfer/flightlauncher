from typing import Annotated
from pathlib import Path
import os
import asyncio
import urllib.parse
import json

from fastapi import FastAPI, Request, HTTPException, Form, WebSocket
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.responses import FileResponse
from sqlalchemy import select, text
import psycopg2

import src.settings as settings
from src.models import Flight, Trace
import src.init as init
from cz_routes import router

from db import async_session

app = FastAPI()
app.include_router(router)

# logs_database_engine = create_async_engine(
#     f'postgresql+asyncpg://{settings.DB_USER}:{settings.DB_PASSWORD}@{settings.DB_HOST}:{settings.DB_PORT}/logs',
#     echo=False
# )
# logs_async_session = async_sessionmaker(logs_database_engine, expire_on_commit=False)

app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")

def dictize(values: tuple, columns: list[str]):
    assert len(values) == 0 or len(values[0]) == len(columns)
    return [
        { key: value[i] for i, key in enumerate(columns) }
        for value in values
    ]

@app.get('/rawimages/{filepath:path}')
async def get_image_data(filepath: str):
    return FileResponse(os.path.join(settings.IMAGES_BASE_DIR, filepath))

@app.get('/rawresults/{filepath:path}')
async def get_image_data(filepath: str):
    return FileResponse(os.path.join(settings.RESULTS_BASE_DIR, filepath))

@app.post("/api/flights/launch")
async def flight_update(message: dict):
    flight_id = int(message['flightId'])

    async with async_session() as session:
        async with session.begin():
            results = await session.execute(
                select(Flight)
                .where(Flight.id == flight_id)
            )
            flight: Flight | None = results.scalar_one_or_none()
            if flight is None:
                raise KeyError(f'Flight {flight_id} does not exists')

            flight.flight_status = 'analysis'
            session.add(flight)
    return { }

@app.get("/", response_class=HTMLResponse)
async def flights_page(request: Request):
    async with async_session() as session:
        async with session.begin():
            results = await session.execute(text(
            '''
                SELECT
                    f.id, f.name, f.flight_status, st.name, st.description
                FROM planning.flight as f
                LEFT JOIN reference.shooting_type as st ON f.shooting_type_id = st.id
                ORDER BY f.id
            '''
            ))
            flights = dictize(results.all(), ['id', 'name', 'status', 'shooting_name', 'shooting_description'])

    return templates.TemplateResponse("flights_page.html", {"request": request, "flights": flights})

@app.get("/create", response_class=HTMLResponse)
async def create_flight_page(request: Request):
    try:
        with psycopg2.connect(dbname=settings.DB_NAME, user=settings.DB_USER,\
            password=settings.DB_PASSWORD, host=settings.DB_HOST, port=settings.DB_PORT) as conn:
                lines = init.get_lines(conn)
                shooting_types = init.get_shooting_types(conn)

        dirs = [os.path.join('/images', dir) for dir in next(os.walk('/images'))[1]]
        return templates.TemplateResponse("flights_create_page.html",
            {
                "request": request,
                "dirs": dirs, 'lines': lines,
                'shooting_types': shooting_types
            })
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


# @app.post("/api/flights/create")
# async def create_flight(message: dict):
#     try:
#         image_dir = message['imageDir']

#         with psycopg2.connect(dbname=settings.DB_NAME, user=settings.DB_USER,\
#             password=settings.DB_PASSWORD, host=settings.DB_HOST, port=settings.DB_PORT) as conn:
#                 job_id = init.insert_job(conn)
#                 images = init.insert_pipeline(conn, image_dir, image_dir, job_id, 'images')

#         return {
#             'images': images
#         }
#     except Exception as e:
#         raise HTTPException(status_code=500, detail=str(e))

@app.post("/api/flights/create")
async def create_flight(
    image_dir: Annotated[str, Form()],
    line_id: Annotated[int, Form()],
    shooting_type_id: Annotated[int, Form()]
):
    print(shooting_type_id)
    try:
        with psycopg2.connect(dbname=settings.DB_NAME, user=settings.DB_USER,\
            password=settings.DB_PASSWORD, host=settings.DB_HOST, port=settings.DB_PORT) as conn:
                job_id = init.insert_job(conn)
                flight_id, images = init.insert_pipeline(conn, image_dir, image_dir, job_id, 'images', shooting_type_id=shooting_type_id)
                init.append_supports(conn, flight_id, line_id)

        return {
            'images': images
        }
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

PAGE_SIZE = 24

@app.get("/flights/{flight_id}", response_class=HTMLResponse)
async def flight_page(request: Request, flight_id: int, page: int | None = None):
    if page is None:
        return RedirectResponse(f'/flights/{flight_id}?page=1')

    try:
        with psycopg2.connect(dbname=settings.DB_NAME, user=settings.DB_USER,\
            password=settings.DB_PASSWORD, host=settings.DB_HOST, port=settings.DB_PORT) as conn:
                images = init.get_images_by_flight(conn, flight_id, offset=((page - 1) * PAGE_SIZE), limit=PAGE_SIZE)
                images_number = init.get_images_count_by_flight(conn, flight_id)
                required_checks = init.get_images_required_checks(conn, flight_id)
                flight_info = init.get_flight_info(conn, flight_id)

        for image in images:
            image['path'] = urllib.parse.quote(image['path'])
            image['required_checks'] = required_checks[image['id']] if image['id'] in required_checks else []

        return templates.TemplateResponse("flight_page.html", {
            'request': request, 'flight_id': flight_id,
            'images_number': images_number, 'pages_number': int(images_number / PAGE_SIZE) + 1,
            'images': images, 'current_page': page, 'flight_info': flight_info,
        })
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

@app.get("/flights/{flight_id}/defects", response_class=HTMLResponse)
async def flight_defects_page(request: Request, flight_id: int):
    async with async_session() as session:
        async with session.begin():
            results = await session.execute(text(
            '''
                SELECT
                    dt.code, dt.name, COUNT(*)
                FROM analysis.defect AS d
                    LEFT JOIN planning.flight_object AS fo ON d.flight_object_id = fo.id
                    LEFT JOIN reference.defect_type AS dt ON d.defect_type_id = dt.id
                WHERE
                    fo.flight_id = :flight_id
                GROUP BY
                    dt.code, dt.name;
            '''
            ), { 'flight_id': flight_id })
            defects = dictize(results.all(), ['id', 'name', 'count'])

    return templates.TemplateResponse("flight_defects_page.html", {"request": request, "defects": defects})


@app.get('/api/flights/{flight_id}/images')
async def get_images(flight_id: int):
    with psycopg2.connect(dbname=settings.DB_NAME, user=settings.DB_USER,\
        password=settings.DB_PASSWORD, host=settings.DB_HOST, port=settings.DB_PORT) as conn:
            images = init.get_images_by_flight(conn, flight_id)

    return images

def get_mask_paths(image_path, image_defects):
    defect_code_dir = os.path.join(*image_path.split('/')[:-1])

    masks_path = []
    for defect in image_defects:
        stem = Path(image_path).stem
        defect_image = os.path.join(
            defect_code_dir, str(defect['code']),
            f'{stem}_defect_{defect["defect_id"]}.png'
        )
        # assert os.path.exists(os.path.join('/results', defect_image))
        masks_path.append(defect_image)

    return masks_path

@app.get("/flights/{flight_id}/images/{image_file_id}", response_class=HTMLResponse)
async def image_defects_page(request: Request, flight_id: int, image_file_id: int):
    with psycopg2.connect(dbname=settings.DB_NAME, user=settings.DB_USER,\
        password=settings.DB_PASSWORD, host=settings.DB_HOST, port=settings.DB_PORT) as conn:
            image_path = init.select_image_file_path(conn, image_file_id)
            defects = init.get_image_defects(conn, flight_id, image_file_id)

    mask_paths = get_mask_paths(image_path, defects)
    mask_paths = [urllib.parse.quote(path) for path in mask_paths]

    for i in range(len(defects)):
        defects[i]['mask_path'] = mask_paths[i]

    return templates.TemplateResponse("image_defects.html", {
        'request': request, 'defects': defects, 'flight_id': flight_id
    })

@app.post("/api/truncate-tasks")
async def truncate_tasks(request: Request):
    with psycopg2.connect(dbname=settings.DB_NAME, user=settings.DB_USER,\
        password=settings.DB_PASSWORD, host=settings.DB_HOST, port=settings.DB_PORT) as conn:
            init.truncate_tasks(conn)

    return { }

# @app.websocket("/ws")
# async def websocket_endpoint(websocket: WebSocket):
#     # TODO: Warning: All clients create futures but never remove them

#     await websocket.accept()
#     while True:
#         message = await MESSAGE_EXCHANGER.get_message()
#         await websocket.send_text(json.dumps({
#             'message': message
#         }))


# @app.get("/traces/flights", response_class=HTMLResponse)
# async def image_defects_page(request: Request):
#     async with logs_async_session() as session:
#         async with session.begin():
#             results = await session.execute(text(
#             '''
#                 SELECT
#                     trace, message, error, payload
#                 FROM
#                     trace
#                 LIKE
                    
#             '''
#             ))
#             traces = dictize(results.all(), ['trace', 'mesasge', 'error', 'payload'])

#     return templates.TemplateResponse("traces.html", {
#         'request': request, 'traces': traces
#     })

# @app.get("/traces", response_class=HTMLResponse)
# async def traces_page(request: Request):
#     async with logs_async_session() as session:
#         async with session.begin():
#             traces = await session.execute(text(
#             '''
#                 SELECT DISTINCT
#                     trace
#                 FROM
#                     trace
#             '''
#             ))
#             traces = traces.scalars()

#     traces = [{
#         'name': t,
#         'url_name': urllib.parse.quote(t)
#     } for t in traces]

#     return templates.TemplateResponse("traces.html", {
#         'request': request, 'traces': traces
#     })

# @app.get("/traces/logs", response_class=HTMLResponse)
# async def image_defects_page(request: Request, tag: str):
#     async with logs_async_session() as session:
#         async with session.begin():
#             results = await session.execute(text(
#             '''
#                 SELECT
#                     trace, message, error, payload
#                 FROM
#                     trace
#                 WHERE
#                     trace = :tag
#             '''
#             ), {'tag': tag})
#             logs = dictize(results.all(), ['trace', 'message', 'error', 'payload'])

#     for t in logs:
#         t['payload'] = json.dumps(t['payload'])

#     return templates.TemplateResponse("trace.html", {
#         'request': request, 'logs': logs
#     })


# @app.post("/api/traces")
# async def image_defects_page(request: list[dict]):
#     async with logs_async_session() as session:
#         async with session.begin():
#             traces = [
#                 Trace(
#                     trace=t['trace'],
#                     message=t['message'],
#                     error=t['error'],
#                     payload=json.loads(t['payload'])
#                 )
#                 for t in request
#             ]
#             session.add_all(traces)

#     return { }
