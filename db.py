from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker

import src.settings as settings

database_engine = create_async_engine(
    f'postgresql+asyncpg://{settings.DB_USER}:{settings.DB_PASSWORD}@{settings.DB_HOST}:{settings.DB_PORT}/{settings.DB_NAME}',
    echo=False
)
async_session = async_sessionmaker(database_engine, expire_on_commit=False)
