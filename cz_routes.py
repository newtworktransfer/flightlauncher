import asyncio

from fastapi import APIRouter, Request
from db import async_session
import sqlalchemy
import pydantic

router = APIRouter()

# class MessageExchanger:
#     def __init__(self) -> None:
#         self.waiters: list[asyncio.Future] = []
    
#     async def get_message(self):
#         future = asyncio.Future()
#         self.waiters.append(future)
#         return await future

#     def push_message(self, message: dict):
#         for waiter in self.waiters:
#             waiter.set_result(message)
        # self.waiters.clear()

class GetEventsRequest(pydantic.BaseModel):
    last_id: int | None

class EventsQueue:
    def __init__(self) -> None:
        self.events = []
        self.last_id = 1

    def get_last_id(self):
        return self.last_id
    
    def get_events_after(self, last_id: int):
        filtered = []
        for id, event in self.events:
            if id > last_id:
                break
            filtered.append(event)
        return filtered

    def append(self, event: dict):
        async def deleter():
            await asyncio.sleep(10)
            self.events.pop(0)

        self.last_id += 1
        self.events.append((self.last_id, event))
        asyncio.create_task(deleter())
        
EVENTS_QUEUE = EventsQueue()


@router.get("/api/events")
async def truncate_tasks(last_id: int | None):
    if last_id:
        return {
            'last_id': EVENTS_QUEUE.get_last_id(),
            'events': EVENTS_QUEUE.get_events_after(last_id)
        }
    else:
        return { 'last_id': EVENTS_QUEUE.get_last_id() }

COUNTER = 1
@router.post("/api/events/test")
async def truncate_tasks():
    global COUNTER
    COUNTER += 1
    EVENTS_QUEUE.append({ 'counter': COUNTER })

async def change_flight_status(flight_id: int, status: str):
    async with async_session() as session:
        async with session.begin():
            return await session.execute(sqlalchemy.text(
            '''
                UPDATE
                    planning.flight
                SET
                    flight_status = :status
                WHERE
                    id = :flight_id
            '''
            ), { 'flight_id': flight_id, 'status': status })


@router.post('/api/flight/{flight_id}/{status}')
async def cz_flight_change_status(flight_id: int, status: str, request: Request):
    if status == 'progress':
        body = await request.json()
        print(body)
        if 'error' not in body:
            progress = body['progress']
            EVENTS_QUEUE.append({
                'event': 'flight-processing-progress',
                'flight_id': flight_id,
                'progress': progress
            })
        else:
            EVENTS_QUEUE.append({ 'event': 'processing-error', 'flight_id': flight_id, 'message': body['error'] })
            await change_flight_status(flight_id, 'planning')
    else:
        if status == 'complete':
            status = 'completed'
        EVENTS_QUEUE.append({ 'event': 'change-flight-status', 'flight_id': flight_id, 'status': status })
        await change_flight_status(flight_id, status)

@router.post('/api/defect/{defect_id}/notify_created')
async def cz_defect_created(defect_id: int):
    EVENTS_QUEUE.append({ 'event': 'defect-created', 'defect_id': defect_id })
    return { }
