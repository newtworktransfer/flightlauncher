const getLogger = () => {
    const body = document.getElementsByTagName('body')[0];
    const base = document.createElement("div");
    body.appendChild(base);

    return {
        log: message => {
            const elem = document.createElement("div");
            elem.innerHTML = message;            
            base.appendChild(elem);
        }
    }
}

logger = getLogger();

const launchButtonHandler = e => {
    const flightId = e.target.dataset.flightId;

    const request = async () => {
        try {
            const response = await fetch('/api/flights/launch', {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify({
                    flightId
                })
            });    

            const body = await response.text();

            if (response.status != 200)
                throw JSON.stringify({ 'message': body })

            window.location.reload();
        }
        catch (e) {
            logger.log(e);
        }
    }

    request();
}

const buttons = [...document.getElementsByClassName('launch-btn')];
buttons.forEach(btn => {
    btn.onclick = launchButtonHandler;
});

const truncateTasks = () => {
    const request = async () => {
        try {
            const response = await fetch('/api/truncate-tasks', { method: "POST" });
            const body = await response.text();

            if (response.status != 200)
                throw JSON.stringify({ 'message': body })

            window.location.reload();
        }
        catch (e) {
            logger.log(e);
        }
    }

    request();
}

const truncate_tasks_btn = document.getElementById('truncate-task-btn');
truncate_tasks_btn.addEventListener('click', truncateTasks)
