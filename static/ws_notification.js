

const List = () => {
    const body = document.getElementsByTagName('body')[0];
    const root = document.createElement('div');
    body.appendChild(root);

    root.style.position = 'absolute';
    root.style.top = '0';
    root.style.left = '0';

    const elements = [];

    const render = () => {
        root.innerHTML = '';
        elements.forEach(text => {
            const div = document.createElement('div');
            div.innerHTML = text;
            root.appendChild(div)
            div.classList.add('message');
        })
    };

    return {
        addMessage: (text) => {
            
            index = elements.push(text);
            render();

            setTimeout(() => {
                elements.shift();
                render();
            }, 20_000)
        }
    };
}

const list = List();


fetch('/api/events?last_id=0')
.then(response => response.json())
.then(initialResponse => {
    let lastId = initialResponse.last_id;

    setInterval(async () => {
        const response = await fetch(`/api/events?last_id=${lastId}`);
        const body = await response.json();
    
        lastId = body.last_id;
        console.log(body);
        body.events.forEach(e => list.addMessage(JSON.stringify(e)));
    }, 5000);
});


// const socket = new WebSocket(`ws://${window.location.host}/ws`);

// socket.onopen = function(e) {
//     console.log("[open] Connection established");
//     console.log("Sending to server");
//     socket.send("My name is John");
// };

// socket.onmessage = function(event) {
//     console.log(`[message] Data received from server: ${event.data}`);
//     list.addMessage(event.data);
// };

// socket.onclose = function(event) {
//     if (event.wasClean) {
//         console.log(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
//     } else {
//         // e.g. server process killed or network down
//         // event.code is usually 1006 in this case
//         console.log('[close] Connection died');
//     }
// };

// socket.onerror = function(error) {
//     console.log(`[error]`);
// };

