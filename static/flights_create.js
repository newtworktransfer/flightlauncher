const getLogger = () => {
    const body = document.getElementsByTagName('body')[0];
    const base = document.createElement("div");
    body.appendChild(base);

    return {
        log: message => {
            const elem = document.createElement("div");
            elem.innerHTML = message;            
            base.appendChild(elem);
        }
    }
}

logger = getLogger();


const imageDirSelect = document.getElementById('image-dir-select');

// const launchButtonHandler = e => {
//     const imageDir = e.target.dataset.imageDir;

//     // console.log(imageDirSelect.value)


//     // const supports = [...document.getElementsByClassName('support-checkbox')];
//     // // console.log(supports);
//     // supports.forEach(e => {
//     //     console.log(e)
//     // });

//     const request = async () => {
//         try {
//             const response = await fetch('/flights/create', {
//                 method: "POST",
//                 headers: {
//                     'Content-Type': 'application/json;charset=utf-8'
//                 },
//                 body: JSON.stringify({
//                     imageDir
//                 })
//             });    

//             const body = await response.text();
//             logger.log(body);
//         }
//         catch (e) {
//             logger.log(e);
//             console.error(e);
//         }
//     }

//     request();
// }


const launchButtonHandler = e => {
    // console.log(imageDirSelect.value)


    // const supports = [...document.getElementsByClassName('support-checkbox')];
    // // console.log(supports);
    // supports.forEach(e => {
    //     console.log(e)
    // });


    e.preventDefault();
    const data = new FormData(e.target);

    const request = async () => {
        try {
            const response = await fetch('/api/flights/create', {
                method: "POST",
                body: data,
            });    

            const body = await response.text();
            logger.log(body);
        }
        catch (e) {
            logger.log(e);
            console.error(e);
        }
    }

    request();
}

console.log(document.getElementById("form"))
document.getElementById("form").addEventListener("submit", launchButtonHandler);


// const buttons = [...document.getElementsByClassName('create-btn')];
// buttons.forEach(btn => {
//     btn.onclick = launchButtonHandler;
// });

