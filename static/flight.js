const getLogger = () => {
    const body = document.getElementsByTagName('body')[0];
    const base = document.createElement("div");
    body.appendChild(base);

    return {
        log: message => {
            const elem = document.createElement("div");
            elem.innerHTML = message;            
            base.appendChild(elem);
        }
    }
};

logger = getLogger();


const root = document.getElementById('root');
const flightId = root.dataset.flightId;

const appendImageAsync = (image) => {
    template = `
        <div class="item-container">
            <div class="image-container">
            <img />
            <div></div>
            </div>
        </div>
    `;
    const container = document.createElement('div');
    container.classList.add('image-container');

    const img = document.createElement('img');
    container.appendChild(img);

    const metadata = document.createElement('div');
    metadata.classList.add('image-metadata');
    metadata.innerHTML = Object.entries(image).reduce((acc, cur) => acc + "<div>" + cur[0] + ": " + cur[1] + "</div>", "");
    container.appendChild(metadata);

    root.appendChild(container);

    img.src = `/rawimages/${image.path}`;
};

const getImageList = async (flightId) => {
    const response = await fetch(`/api/flights/${flightId}/images`, { method: "GET" });
    return images = await response.json();
};

// getImageList(flightId).then(images => {
//     console.log(images)
//     for (let i = 0; i < (images.length > 20 ? 20 : images.length); ++i) {
//         appendImageAsync(images[i]);
//     }
// });



