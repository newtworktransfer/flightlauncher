.PHONY: image
image:
	docker build --rm --no-cache -t flight-launcher .

.PHONY: run
run:
	docker run --rm --name fligth-launcher -p 8000:80 -e DB_PASSWORD=postgres -e DB_HOST=10.24.65.227 -v /images:/images flight-launcher
